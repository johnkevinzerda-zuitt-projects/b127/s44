const getData = () => {
	fetch('https://jsonplaceholder.typicode.com/users')
	.then(response => {
		return response.json();
	}).then(data => {
		let users = "";
		data.forEach(user => {
			users += `
			<div>
				<div class="card-group">
					<div class="card">
						<img src="p1.jpg" class="card-img-top-thumbnail" alt="...">
						<div class="card-body">
							<h3>Name: ${user.name}</h3>
							<p>Email: ${user.email}</p>
							<p>Company: ${user.company}</p>
							<p>Phone: ${user.phone}</p>
						</div>
					</div>
				</div>
			</div>
			`
		});
		document.querySelector('#output').innerHTML = users;
	})
	.catch(err => {
		console.log(err)
	});
}

const addUser = (e) => {
	e.preventDefault();
	let name = document.querySelector('#name').value
	let username = document.querySelector('#username').value
	let email = document.querySelector('#email').value
	let street = document.querySelector('#street').value
	let suite = document.querySelector('#suite').value
	let city = document.querySelector('#city').value
	let zipcode = document.querySelector('#zipcode').value
	let lat = document.querySelector('#lat').value
	let lng = document.querySelector('#lng').value
	let phone = document.querySelector('#phone').value
	let website = document.querySelector('#website').value
	let companyName = document.querySelector('#companyName').value
	let catchphrase = document.querySelector('#catchPhrase').value
	let bs = document.querySelector('#bs').value

	fetch('https://jsonplaceholder.typicode.com/users', {
		method: 'POST',
		headers: {
			'Accept': 'application/json, text/plain',
			'Content-type': 'application/json'
		},
		body: JSON.stringify({
			name: name,
			username: username,
			email: email,
			address: {
				street: street,
				suite: suite,
				city: city,
				zipcode: zipcode,
				geo: {
					lat: lat,
					lng: lng
				}
			},
			phone : phone,
			website: website,
			company: {
				name: companyName,
				catchphrase: catchphrase,
				bs: bs
			}
		})
	})
	.then(response => response.json())
	.then(data => console.log(data))
}

const btn1 = document.querySelector('#btn1');
btn1.addEventListener('click', getData);

const addForm = document.querySelector('#addUser');
addForm.addEventListener('submit', addUser);